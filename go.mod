module gitlab.com/shidfar/imdb-insights

go 1.13

require (
	github.com/cenk/backoff v2.2.1+incompatible // indirect
	github.com/facebookgo/clock v0.0.0-20150410010913-600d898af40a
	github.com/fsnotify/fsnotify v1.4.9
	github.com/gorilla/mux v1.8.0
	github.com/hashicorp/hcl v1.0.0
	github.com/magiconair/properties v1.8.5
	github.com/mitchellh/mapstructure v1.4.1
	github.com/newrelic/go-agent v3.11.0+incompatible
	github.com/pelletier/go-toml v1.9.0
	github.com/rs/cors v1.7.0
	github.com/rs/dnscache v0.0.0-20210201191234-295bba877686
	github.com/rubyist/circuitbreaker v2.2.1+incompatible
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/afero v1.6.0
	github.com/spf13/cast v1.3.1
	github.com/spf13/jwalterweatherman v1.1.0
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.1
	github.com/subosito/gotenv v1.2.0
	gitlab.com/shidfar/goutils v0.0.0-20210502141106-d53e012c9d77
	go.mongodb.org/mongo-driver v1.5.1
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/sys v0.0.0-20210426230700-d19ff857e887
	golang.org/x/text v0.3.6
	golang.org/x/tools v0.0.0-20191112195655-aa38f8e97acc
	gopkg.in/ini.v1 v1.62.0
	gopkg.in/yaml.v2 v2.4.0
)
