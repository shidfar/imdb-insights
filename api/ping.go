package api

import (
	"fmt"
	"net/http"
)

func init() {
	router.HandleFunc("/ping", pingHandler).Methods(http.MethodGet)
}

func pingHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Pong")
}
