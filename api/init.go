package api

import (
	"context"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	circuit "github.com/rubyist/circuitbreaker"
	"github.com/sirupsen/logrus"
	"gitlab.com/shidfar/imdb-insights/lib"
	"gitlab.com/shidfar/imdb-insights/utils"
	"gitlab.com/shidfar/imdb-insights/db"
)

// const defaultTimeout string = ""

var (
	imdbClient *db.MongoClient
	config     *lib.ConfigSchema
	log        *logrus.Logger
	router     = mux.NewRouter()
	cb         = circuit.NewRateBreaker(0.95, 100)
	httpClient = circuit.NewHTTPClientWithBreaker(cb, 0, utils.GetHTTPClient())
)

// Configure configures the main app
func Configure(conf *lib.ConfigSchema, logger *logrus.Logger, imdb *db.MongoClient) *http.Handler {
	config = conf
	log = logger
	imdbClient = imdb
	c := cors.New(cors.Options{
		AllowedOrigins:   config.Cors.AllowedOrigins,
		AllowCredentials: true,
		AllowedMethods:   []string{http.MethodGet, http.MethodPost},
	})
	handler := c.Handler(router)
	return &handler
}

// Begin creates a timeout ctx from the request ctx
func Begin(name string, r *http.Request, customTimeout string) (*context.Context, *lib.Resources, func() error) {
	var timeout time.Duration
	var err error
	if customTimeout != "" {
		timeout, err = time.ParseDuration(customTimeout)
		if err != nil {
			panic("could not parse customTimeout string")
		}
	} else {
		timeout = time.Duration(config.DefaultRequestTimeoutSeconds)
	}
	ctx, cancel := context.WithTimeout(r.Context(), timeout*time.Second)

	res := lib.Resources{
		Log:        log,
		Config:     config,
		HTTPClient: httpClient,
	}

	cleanup := func() error {
		cancel()
		return err
	}

	return &ctx, &res, cleanup
}
