package api

import (
	"fmt"
	"net/http"
)

func init() {
	router.HandleFunc("/getreviews", getReviewsCount).Methods(http.MethodGet)
}

func getReviewsCount(w http.ResponseWriter, r *http.Request) {
	stat, err := imdbClient.GetStatistics()

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Uh oh something is wrong.")
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "%s", stat)
	}
}
