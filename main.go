package main

import (
	"gitlab.com/shidfar/goutils"
	"gitlab.com/shidfar/imdb-insights/api"
	"gitlab.com/shidfar/imdb-insights/db"
	"gitlab.com/shidfar/imdb-insights/lib"
)

func main() {
	// get our config and logger setup
	config := lib.LoadConfig()
	logger := config.Logger.Get()

	mongoClient, _ := db.GetMongoDbClient(&config)

	handler := api.Configure(&config, logger, &mongoClient)

	// start serving
	server := goutils.MultiServer(logger)
	go server.Add("REST API", handler, config.Port, config.Host)

	// Wait for shutdown to close
	<-server.Shutdown
	logger.Info("Gracefully shutdown")
}
