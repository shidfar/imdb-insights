package utils

import (
	"context"
	"net"
	"net/http"
	"strings"
	"time"

	"github.com/rs/dnscache"
)

var r = &dnscache.Resolver{}

// GetHTTPClient creates an http client with proper timeouts
func GetHTTPClient() *http.Client {
	go func() {
		t := time.NewTicker(5 * time.Minute)
		defer t.Stop()
		for range t.C {
			r.Refresh(true)
		}
	}()
	return netClient
}

var netTransport = &http.Transport{
	MaxIdleConnsPerHost: 64,
	DialContext: func(ctx context.Context, network string, addr string) (conn net.Conn, err error) {
		separator := strings.LastIndex(addr, ":")
		ips, err := r.LookupHost(ctx, addr[:separator])
		if err != nil {
			return nil, err
		}
		for _, ip := range ips {
			conn, err = net.Dial(network, ip+addr[separator:])
			if err == nil {
				break
			}
		}
		return
	},
	TLSHandshakeTimeout: 5 * time.Second,
}

var netClient = &http.Client{
	Timeout:   time.Second * 10,
	Transport: netTransport,
}
