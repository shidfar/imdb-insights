import pymongo

from appcontext import *

def getDbClient():
    dbClient = pymongo.MongoClient(MONGOSERVER)
    mydb = dbClient[DB_NAME]

    usersCollection = mydb[USERS_COLLECTION]
    moviesCollection = mydb[MOVIES_COLLECTION]
    reviewsCollection = mydb[REVIEWS_COLLECTION]

    def countReviews():
        return reviewsCollection.count_documents({})    

    return new('IMDb', {
        'countReviews' : countReviews,
    })
