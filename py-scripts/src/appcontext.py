DB_NAME = 'imdb'
USERS_COLLECTION = 'users'
MOVIES_COLLECTION = 'movies'
REVIEWS_COLLECTION = 'reviews'

MONGOSERVER = 'mongodb://127.0.0.1:27017'

def new(name, data):
  return type(name, (object,), data)

def makeFibonacci():
  def plus(a, b):
    return (b, a + b)
  x1 = 5
  x2 = 8
  sequence = [5, 8]
  for i in range(11):
    (x1, x2) = plus(x1, x2)
    sequence.append(x2)
  return sequence
