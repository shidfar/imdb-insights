package db

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"time"

	"gitlab.com/shidfar/imdb-insights/lib"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoClient struct {
	reviews    mongo.Collection
	users      mongo.Collection
	movies     mongo.Collection
	context    mongo.SessionContext
	client     mongo.Client
	cancelFunc context.CancelFunc
	ClientName string
}

type StatEntity struct {
	Visited    int64
	NotVisited int64
}

type StatistResponse struct {
	Reviews int64
	Users   StatEntity
	Movies  StatEntity
}

func GetMongoDbClient(conf *lib.ConfigSchema) (MongoClient, error) {
	mongoServerUri := fmt.Sprint("mongodb://", conf.MongoDb.Host, ":", conf.MongoDb.Port)
	client, err := mongo.NewClient(options.Client().ApplyURI(mongoServerUri))
	if err != nil {
		log.Fatal(err)
		return MongoClient{}, errors.New("unable to connect to mongo")
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
		client.Disconnect(ctx)
		cancel()
		return MongoClient{}, errors.New("unable to connect to mongo")
	}

	imdb := client.Database("imdb")
	return MongoClient{
		reviews:    *imdb.Collection("reviews"),
		users:      *imdb.Collection("users"),
		movies:     *imdb.Collection("movies"),
		client:     *client,
		cancelFunc: cancel,
		ClientName: "MongoIMDb",
	}, nil
}

func doCountingOn(col mongo.Collection, ctx context.Context, filter interface{}) <-chan int64 {
	res := make(chan int64)

	go func(col mongo.Collection, ctx context.Context, filter interface{}) {
		defer close(res)
		count, err := col.CountDocuments(ctx, filter)
		if err != nil {
			log.Fatal("could not retrieve document count")
			res <- (-1)
		}
		res <- count
	}(col, ctx, filter)

	return res
}

func (client MongoClient) GetStatistics() (string, error) {
	reviewsCountChan := doCountingOn(client.reviews, client.context, bson.D{})
	visitedMoviesCountChan := doCountingOn(client.movies, client.context, bson.M{"visited": true})
	notVisitedMoviesCountChan := doCountingOn(client.movies, client.context, bson.M{"visited": false})
	visitedUsersCountChan := doCountingOn(client.users, client.context, bson.M{"visited": true})
	notVisitedUsersCountChan := doCountingOn(client.users, client.context, bson.M{"visited": false})

	reviewsCount := <-reviewsCountChan
	visitedMoviesCount := <-visitedMoviesCountChan
	notVisitedMoviesCount := <-notVisitedMoviesCountChan
	visitedUsersCount := <-visitedUsersCountChan
	notVisitedUsersCount := <-notVisitedUsersCountChan
	// distinctMovieReviews, err := client.reviews.Distinct(client.context, "movieId", nil)

	res := StatistResponse{
		Reviews: reviewsCount,
		Users: StatEntity{
			Visited:    visitedUsersCount,
			NotVisited: notVisitedUsersCount,
		},
		Movies: StatEntity{
			Visited:    visitedMoviesCount,
			NotVisited: notVisitedMoviesCount,
		},
	}

	jsonRes, err := json.Marshal(res)

	if err != nil {
		return "", err
	}

	return string(jsonRes), nil
}
