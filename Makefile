install:
	@dep ensure

build:
	@docker build -t imdb-insights -f Dockerfile.build .

run:
	@docker run --rm -it -p 8001:8001 imdb-insights ./imdb-insights

test:
	@go test -v ./...
