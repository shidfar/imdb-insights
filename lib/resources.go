package lib

import (
	"github.com/rubyist/circuitbreaker"
	"github.com/sirupsen/logrus"
)

// Resources type is a bundle of databases, loggers and other things
// that gets passed to every lib function, after the context object.
type Resources struct {
	Log        *logrus.Logger
	Config     *ConfigSchema
	HTTPClient *circuit.HTTPClient
}
