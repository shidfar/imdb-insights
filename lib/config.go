package lib

import "gitlab.com/shidfar/goutils"

// ConfigSchema define a struct to load our config into
type ConfigSchema struct {
	Port                         int
	Host                         string
	DefaultRequestTimeoutSeconds int
	Logger                       goutils.LoggerConfig
	Cors                         ConfigCors
	MongoDb                      MongoClientConfig
}

type ConfigCors struct {
	AllowedOrigins []string
}

type MongoClientConfig struct {
	Port                  int
	Host                  string
	RequestTimeoutSeconds int
}

func LoadConfig() ConfigSchema {
	var config ConfigSchema
	goutils.LoadConfig(&config, "./config")
	return config
}
